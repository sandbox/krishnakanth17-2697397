<?php
/**
 * @file
 * Views include file with views hooks.
 */

/**
 * Implementation of hook_views_plugins().
 */
function watchdog_backup_views_plugins() {
  $path = drupal_get_path('module', 'watchdog_backup');

  $style_defaults = array(
    'path' => $path . '/plugins',
    'parent' => 'watchdog_backup',
    'theme' => 'watchdog_backup',
    'theme path' => $path . '/theme',
    'theme file' => 'watchdog_backup.theme.inc',
    'uses row plugin' => FALSE,
    'uses fields' => TRUE,
    'uses options' => TRUE,
    'type' => 'data_export',
  );

  return array(
    'style' => array(
      'watchdog_backup_sql' => array(
          'title' => t('SQL file'),
          'help' => t('Get a view a sql file.'),
          'handler' => 'watchdog_backup_plugin_style_export_sql',
          // Views Data Export element that will be used to set additional headers when serving the feed.
          'export headers' => array('Content-type' => 'text/csv; charset=utf-8'),
          // Views Data Export element mostly used for creating some additional classes and template names.
          'export feed type' => 'sql',
          'export feed text' => 'SQL',
          'export feed file' => '%view.sql',
          // 'export feed icon' => drupal_get_path('module', 'views_data_export') . '/images/csv.png',
          'additional themes' => array(
            'watchdog_backup_sql_header' => 'style',
            'watchdog_backup_sql_body' => 'style',
            'watchdog_backup_sql_footer' => 'style',
          ),
          /*'additional themes base' => 'watchdog_backup_sql',*/
        ) + $style_defaults,
    ),
  );
}
